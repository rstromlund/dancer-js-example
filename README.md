# LAMP Dancer REST JS 1Page Example

A quick and light weight LAMP example of a 1-page javascript web application complete w/ REST service.  The REST service is a perl program written using the Dancer framework.

Note: this only supports, for now, CRU. The project this is a prototype for disallows Delete.

Note: I am not an MVC expert but I did attempt to split JS code into logical domains; a lot of refactoring may be needed to make this prod ready.

Note: start service simply via $ ./svc/svc.pl; or $ cd svc && ./svc.pl; then load the index.html file into your browser from "http://127.0.0.1/dancer-js-example/".  If you use values other then the default, you will most likely need to modify the REST_URL variable in "controller.js".