'use strict';

// loopback or ip address (and dns?), either is good as long as your browser will accept cookies and svc.pl will "Allow" that origin.
var REST_URL = 'http://127.0.0.1:3000';


var Control = {
	// REST/HTTP helper function:

	_leaveWarningAttached: 0,

	addLeaveWarning: function() {
		if (0 == this._leaveWarningAttached) {
			window.addEventListener('beforeunload', (event) => {
				if (0 != vw.getNewModRecords().length) {event.returnValue = 'You have unsaved changed, are you sure you want to leave?';}
			});
			this._leaveWarningAttached = 1;
		}
	},

	createCORSRequest: function(method, url, isblocking) {
		var xhr = new XMLHttpRequest();
		if ('withCredentials' in xhr) {

			// Check if the XMLHttpRequest object has a "withCredentials" property.
			// "withCredentials" only exists on XMLHTTPRequest2 objects.
			xhr.open(method, url, !isblocking);
			xhr.withCredentials = true;

		} else if (typeof XDomainRequest != 'undefined') {

			// Otherwise, check if XDomainRequest.
			// XDomainRequest only exists in IE, and is IE's way of making CORS requests.
			xhr = new XDomainRequest();
			xhr.open(method, url);

		} else {

			// Otherwise, CORS is not supported by the browser.
			xhr = null;

		}
		return xhr;
	},


	// Auth functions:

	login: function(btn) {
		// FIXME: since this is an 'id', we dont really need to follow the element clicked. Is it good practice to do it though?
		var u = btn.parentElement.parentElement.querySelector('#userid').value;
		var p = btn.parentElement.parentElement.querySelector('#passwd').value;
		if (!u || !p || '' == u || '' == p) {
			alert('Please supply both a username and password');
			return(false);
		}

		var request = this.createCORSRequest('GET', REST_URL + '/login?username=' + u + '&password=' + p);
		if (! request) {throw new Error('CORS not supported');}

		request.onload = function () {
			var data;
			try {
				data = JSON.parse(this.response);
			}
			catch(err) {
				data = {error: this.response};
			}

			if (this.status >= 200 && this.status < 400 && data.ok) {
				var s = this.getResponseHeader('X-My-Secret');
				// This isn't really a "jsessionid" ... but just play along; it looks legit:
				window.location.href = 'webapp.html?jsessionid=' + s;
			} else {
				console.log(this);
				alert('ERROR ' + data.error + ' -- ' + this.status);
			}
		};

		request.onerror = function() {
			console.log(this);
			alert('ERROR ' + this.response + ' -- ' + this.status);
		};

		request.send();
		return(false);

	},

	logout: function() {
		var request = this.createCORSRequest('GET', REST_URL + '/logout');
		if (! request) {throw new Error('CORS not supported');}
		var urlParams = new URLSearchParams(window.location.search);
		request.setRequestHeader('X-Your-Secret', urlParams.get('jsessionid'));

		request.onload = function () {
			var data;
			try {
				data = JSON.parse(this.response);
			}
			catch(err) {
				data = {error: this.response};
			}

			if (this.status >= 200 && this.status < 400 && data.ok) {
				window.location.href = './';
			} else {
				console.log(this);
				alert('ERROR ' + data.error + ' -- ' + this.status);
			}
		};

		request.onerror = function() {
			console.log(this);
			alert('ERROR ' + this.response + ' -- ' + this.status);
			window.location.href = './';
		};

		request.send();
		return(false);

	}

};

var db = Object.create(Database);
var vw = Object.create(View);
var ctl = Object.create(Control);
