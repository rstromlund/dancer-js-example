'use strict';

var Person = {
	state: 'new',
	keyid: 'e000000',
	lastname: '*lastname*',
	firstname: '*firstname*',

	match: function(re) {
		return(this.lastname.match(re) || this.firstname.match(re));
	},

	update: function(r) {
		if (this.keyid != r.keyid) return(false); // wrong record to update
		if (this.lastname == r.lastname && this.firstname == r.firstname) return(false); // nothing to do

		this.lastname = r.lastname;
		this.firstname = r.firstname;
		this.state = 'ok';
		return(true);
	}
};

var Database = {
	_rest: function(m, u, d) {
		//FIXME: we should not be making blocking calls, not at least on the main thread.  It is ok for a mock up ... I guess.
		//FIXME: do not promote blocking code to PROD.
		//FIXME: perhaps io stuff belongs in controller or view?

		var request = ctl.createCORSRequest(m, u, true);
		if (! request) {throw new Error('CORS not supported');}
		var urlParams = new URLSearchParams(window.location.search);
		request.setRequestHeader('X-Your-Secret', urlParams.get('jsessionid'));
		request.setRequestHeader('Content-Type', 'application/json');

		try {
			request.send(d);
		} catch(err) {
			console.log(err);
			console.log(request);
			window.location.href = './';
		}

		return(request);
	},

	search: function(s) {
		var ms = [];
		var request = this._rest('GET', REST_URL + '/Search?term=' + s);
		var data;
		try {
			data = JSON.parse(request.response);
		}
		catch(err) {
			data = {error: request.response};
		}

		if (request.status >= 200 && request.status < 400 && data.ok) {
			for (var key in data.ok) {
				var j = data.ok[key];
				var p = Object.create(Person);
				p.keyid = key;
				p.lastname = j.lastname;
				p.firstname = j.firstname;
				ms.push(p);
			}

		} else {
			console.log(request);
			alert('ERROR ' + data.error + ' -- ' + request.status);
			window.location.href = './';
		}

		return(ms);
	},

	insert(p) {
		var j = {id: p.keyid, lastname: p.lastname, firstname: p.firstname};
		var request = this._rest('POST', REST_URL + '/Employee', JSON.stringify(j));
		var data;
		try {
			data = JSON.parse(request.response);
		}
		catch(err) {
			data = {error: request.response};
		}

		if (request.status >= 200 && request.status < 400 && data.ok) {
			p.state = 'ok';
			return(true);
		}
		console.log(request);
		alert('ERROR ' + data.error + ' -- ' + request.status);
		if (request.status >= 400 && request.status < 500) {window.location.href = './';}
		return(false);
	},

	update(p) {
		var j = {id: p.keyid, lastname: p.lastname, firstname: p.firstname};
		var request = this._rest('PUT', REST_URL + '/Employee', JSON.stringify(j));
		var data;
		try {
			data = JSON.parse(request.response);
		}
		catch(err) {
			data = {error: request.response};
		}

		if (request.status >= 200 && request.status < 400 && data.ok) {
			p.state = 'ok';
			return(true);
		}

		console.log(request);
		alert('ERROR ' + data.error + ' -- ' + request.status);
		if (request.status >= 400 && request.status < 500) {window.location.href = './';}
		return(false);
	}
};
