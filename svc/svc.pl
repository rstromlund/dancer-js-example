#!/usr/bin/env perl
use Dancer;
use strict;

use constant SESSION_TIMEOUT	=> 600; # 10 minute timeout
use constant AUTH_USERNAME		=> 'Aristotle';
use constant AUTH_PASSWORD		=> 'Greece.384BC!';
use constant ALLOW_METHODS		=> 'GET, PUT, POST'; # For this mockup, DELETE is denied.

use constant REST_TOKEN_HDR_OUT						=> 'X-My-Secret';
use constant REST_TOKEN_HDR_IN						=> 'X-Your-Secret';
use constant ACCESS_CONTROL_ALLOW_CREDENTIALS	=> 'true';
use constant ACCESS_CONTROL_ALLOW_ORIGIN_1		=> 'http://127.0.0.1';
use constant ACCESS_CONTROL_ALLOW_ORIGIN_2		=> 'http://localhost';
use constant ACCESS_CONTROL_EXPOSE_HEADERS		=> REST_TOKEN_HDR_OUT;
use constant ACCESS_CONTROL_ALLOW_HEADERS			=> 'Content-Type, ' . REST_TOKEN_HDR_IN;

# dancer config (consider a config file in a "real" application).
set 'serializer'	=> 'JSON';
set 'session'		=> 'Simple';
set 'logger'		=> 'console';
set 'log'			=> 'error'; # error, debug, core;
set 'show_errors'	=> true;


# In memory database, "svc.pl" is a REST address book ... well not addresses really, more of a redis name store like thing.
my $db = {
	'e1879' => {
		'lastname' => 'Einstein',
		'firstname' => 'Albert'
	},
	'e1857' => {
		'lastname' => 'Hertz',
		'firstname' => 'Heinrich'
	}
};


# Add apropos headers and validate session

hook 'before' => sub {
	#my $route_handler = shift; use Data::Dumper qw(Dumper); debug(' ***** Before:', Dumper($route_handler));
	my $o = (request->header('Origin') || 'na');
	debug(' ***** ', request->method, ': ', request->path_info);
	debug(' ***** Origin: ', $o);

	# CORS required headers
	headers
		'Access-Control-Allow-Credentials' => ACCESS_CONTROL_ALLOW_CREDENTIALS,
		'Access-Control-Expose-Headers' => ACCESS_CONTROL_EXPOSE_HEADERS,
		'Access-Control-Allow-Headers' => ACCESS_CONTROL_ALLOW_HEADERS,
		'Access-Control-Allow-Methods' => ALLOW_METHODS,
	;
	header 'Access-Control-Allow-Origin' => $o  if (ACCESS_CONTROL_ALLOW_ORIGIN_1 eq $o || ACCESS_CONTROL_ALLOW_ORIGIN_2 eq $o);

	my $t = 0 + (session('session_time') || 0);
	my $s = session('secret') || 'na';

	if ('/login' ne request->path_info && '/logout' ne request->path_info && 'OPTIONS' ne request->method) {
		if (! defined $t || $t < 1) {
			debug(" ***** ! defined $t || $t < 1");
			header 'WWW-Authenticate' => 'AuthRejected auth-param="please-login"';
			status 401; halt {'error' => 'Not logged in'};
		}
		elsif (time() - $t > SESSION_TIMEOUT) {
			debug(" ***** time() - $t > SESSION_TIMEOUT");
			session->destroy;
			header 'WWW-Authenticate' => 'AuthRejected auth-param="session-timeout"';
			status 401; halt {'error' => 'session timeout'};
		}
		elsif ($s ne (request->header(REST_TOKEN_HDR_IN) || 'na')) {
			debug(" ***** ($s ne (request->header(REST_TOKEN_HDR_IN) || 'na')");
			header 'WWW-Authenticate' => 'AuthRejected auth-param="missing-secret"';
			status 401; halt {'error' => 'Shhh, it\'s a secret'};
		}
		else {
			# Session is valid, freshen up our time limit.
			session 'session_time' => time()  if($t > 0);
		}
	}

};

#hook 'after' => sub {
#	my $response = shift;
#	use Data::Dumper qw(Dumper); debug(' ***** After:', Dumper($response));
#};

options '/*' => sub {
	header 'Allow' => ALLOW_METHODS;
	status 204; # 204 = no content (i.e. no 'send' or return command).
};

del '/*' => sub {
	send_error('Not allowed', 403);
};


# Main REST verbs (CRU, no D) here:

get '/login' => sub {
	if (AUTH_USERNAME ne param('username') || AUTH_PASSWORD ne param('password')) {
		session->destroy;
		header 'WWW-Authenticate' => 'AuthRejected auth-param="invalid-login"';
		status 401; return {'error' => 'unknown username/password. Hint: username/password = ' . AUTH_USERNAME . '/' . AUTH_PASSWORD};
	}

	# Company standards requires an Access Token above and beyond login authorization to be passed on every REST call.
	my $secret = 'secret-' . rand(65535);
	session 'session_time'	=> time();
	session 'secret'			=> $secret;
	header scalar(REST_TOKEN_HDR_OUT) => $secret;
	return {'ok' => 'authorized'};
};

get '/logout' => sub {
	session->destroy;
	return {'ok' => 'logged out'};
};

get '/Employees' => sub {
	return $db;
};

get '/Employee' => sub {
	return $db->{param('id')};
};

put '/Employee' => sub {
	my $ck = $db->{param('id')};
	if (! $ck) {return {'error' => 'id does not exist -- ' . param('id')}};

	$db->{param('id')}->{'lastname'}		= param('lastname');
	$db->{param('id')}->{'firstname'}	= param('firstname');
	return {'ok' => 'complete'};
};

post '/Employee' => sub {
	my $ck = $db->{param('id')};
	if ($ck) {return {'error' => 'id exists -- ' . param('id')}};

	$db->{param('id')} = {'lastname' => param('lastname'), 'firstname' => param('firstname')};
	return {'ok' => 'complete'};
};

get '/Search' => sub {
	my $term = param('term');
	my $rs = {};
	while (my ($key, $value) = each (%{$db}))
	{
		$rs->{$key} = $value  if ($value->{'firstname'} =~ m/$term/i || $value->{'lastname'} =~ m/$term/i);
	}
	return {'ok' => $rs};
};

dance;
