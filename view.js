'use strict';

var View = {
	_inputChanged: function(e) {
		e.srcElement.parentElement.parentElement.className = 'update';
		ctl.addLeaveWarning();
	},

	_getDataTable: function() {
		var t = document.querySelector('#data-table');
		if (t.querySelector('#deleteMe')) {t.querySelector('tbody').deleteRow(0);}
		return(t);
	},

	_refreshCnt: function() {
		var t = this._getDataTable();
		var tb = t.querySelector('tbody');
		var cnt = tb.getElementsByTagName('tr').length;
		t.querySelector('tfoot>tr>th#person-cnt').innerHTML = cnt;
		return(cnt);
	},

	_insertRow: function(c, p) {
		var tr = document.createElement('tr');
		tr.className = c;

		var keyField = ('insert' == c) ? '<input type="text" class="keyid" name="keyid" value="' + p.keyid + '" />' : p.keyid;
		tr.innerHTML = '<th scope="row">' + keyField + '</th><td><input type="text" class="lastname" name="lastname" value="' + p.lastname + '" /></td><td><input type="text" class="firstname" name="firstname" value="' + p.firstname + '" /></td>';

		var t = this._getDataTable();
		var tb = t.querySelector('tbody');
		tb.appendChild(tr);

		this._refreshCnt();
		return(tr);
	},

	_attachChangeListener: function(tr) {
		var is = tr.getElementsByTagName('input');
		for (var i = 0; i < is.length; i ++) {
			is[i].addEventListener('change', View._inputChanged);
		}
		return(true);
	},

	tableAddPerson: function(c, p) {
		var tr = this._insertRow(c, p);
		this._attachChangeListener(tr);
		return(true);
	},

	tableNewPerson: function(c) {
		var tb = this._getDataTable().querySelector('tbody');
		var pcnt = tb.getElementsByTagName('tr').length;

		var id = (10000 + pcnt).toString();

		var p = Object.create(Person);
		p.state = c;
		p.keyid = 'e' + id;
		p.lastname = '*last-' + id;
		p.firstname = '*first-' + id;
		this._insertRow(c, p);
		ctl.addLeaveWarning();

		return(false);
	},

	getNewModRecords: function() {
		// Get records that have been inserted/updated
		var trs = [];

		var is = this._getDataTable().querySelector('tbody').getElementsByClassName('insert');
		for (var i = 0; i < is.length; i ++) {trs.push(is[i]);}

		var us = this._getDataTable().querySelector('tbody').getElementsByClassName('update');
		for (var i = 0; i < us.length; i ++) {trs.push(us[i]);}

		return(trs);
	},

	searchRecs: function(btn) {
		if (0 != this.getNewModRecords().length) {alert('Unsaved changes, exiting search.'); return(false);}

		// FIXME: since this is an 'id', we dont really need to follow the element clicked. Is it good practice to do it though?
		var ms = db.search(btn.parentElement.parentElement.querySelector('#searchText').value);
		document.querySelector('#data-table>tbody').innerHTML = '';
		if (ms.length > 0) {
			for (var i = 0; i < ms.length; i ++) {
				this.tableAddPerson('ok', ms[i]);
			}
		}
		this._refreshCnt(); // if 0 == length, note that.
		return(false);
	},

	saveUpdate: function() {
		var n = 0, u = 0, e = 0; // iNsert, Update, Error counts
		var trs = this.getNewModRecords();

		for (var i = 0; i < trs.length; i ++) {
			var tr = trs[i];
			var p = Object.create(Person);
			p.state = '' != tr.className ? tr.className : 'ok';
			p.keyid = ('insert' == p.state) ? tr.querySelector('.keyid').value : tr.querySelector('th').innerHTML;
			p.lastname = tr.querySelector('.lastname').value;
			p.firstname = tr.querySelector('.firstname').value;

			if ('insert' == p.state) {
				if (db.insert(p)) {
					tr.className = p.state;
					tr.querySelector('th').innerHTML = p.keyid;
					this._attachChangeListener(tr);
					n ++;

				} else {
					e ++;
				}

			} else if ('update' == p.state) {
				if (db.update(p)) {
					tr.className = p.state;
					u ++;

				} else {
					e ++;
				}

			}

		}

		if (0 != n + u + e) {alert('Done, INSERT ' + n + ' new rows, UPDATE ' + u + ' existing rows, ERRORS ' + e);}
		return(false);

	}

};
